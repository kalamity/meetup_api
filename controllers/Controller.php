<?php

abstract class Controller {
    public function getHttpData() {
        // GET
        if ($_SERVER['REQUEST_METHOD'] == 'GET') return $_GET;
        // POST
        if ($_SERVER['REQUEST_METHOD'] == 'POST') return $_POST;
        // PUT / PATCH / DELETE
        parse_str(file_get_contents("php://input"),$data);
        return $data;
     }
}