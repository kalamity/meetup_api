<?php

require_once 'controllers/Controller.php';
require_once 'models/Meetup.php';

class MeetupController extends Controller {

    //Read
    public function getAll() {
        $meetup = new Meetup();
        return json_encode($meetup->getAll());
    }

    public function getMeetup($id) {
        $meetup = new Meetup();
        return json_encode($meetup->getMeetup($id));
    }
    //Create
    public function create() {
        $title = $_POST['title'];
        $description = $_POST['description'];
        $speaker=$_POST['speaker'];
        $location = $_POST['location'];
        $image = $_POST['image'];
        $date=$_POST['date'];
        //Pour éviter les requétes JS
        $title = htmlspecialchars($title);
        $speaker = htmlspecialchars($speaker);
        $description = htmlspecialchars($description);
        $location = htmlspecialchars($location);
        $image =htmlspecialchars($image);
        $date = htmlspecialchars($date);

        $meetup = new Meetup();
        return json_encode($meetup->create($title,$speaker,$description,$location,$image,$date));
    }
    //Update
    function update() {
        $data = $this->getHttpData();
        $id=$data['id'];
        $title = $data['title'];
        $speaker=$data ['speaker'];
        $description = $data['description'];
        $location = $data['location'];
        $image=$data['image'];
        $date=$data['date'];

        //Pour éviter les requétes JS
        $title = htmlspecialchars($title);
        $description = htmlspecialchars($description);
        $speaker = htmlspecialchars($speaker);
        $location = htmlspecialchars($location);
        $date = htmlspecialchars($date);

        $meetup = new Meetup();
        return json_encode($meetup->update( $id,$title,$speaker,$description,$location,$image,$date));
    }

       //Delete
       function delete($id) {
        $meetup = new meetup();
        return json_encode($meetup->delete($id));
    }

}



?>