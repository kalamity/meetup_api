<?php
require_once 'controllers/Controller.php';
require_once 'models/Location.php';

class LocationController extends Controller {

    //Get All the location
    function getAll() {
        $location = new Location();
        return json_encode($location->getAll());
    }

    //Create a new location
    function create(){
        $city = $_POST['city'];
        $address = $_POST['address'];
        $zip_code = $_POST['zip_code'];
        $description = $_POST['description'];
        //Pour éviter les requétes JS
        $address = htmlspecialchars($address);
        $city = htmlspecialchars($city);
        $zip_code= htmlspecialchars($zip_code);
        $description = htmlspecialchars($description);

        $location = new Location();
        return json_encode($location->create($city,$address,$zip_code,$description));
    }

    //Delete location
    function delete($id) {
        $location = new Location();
        return json_encode($location->delete($id));
    }

    function update() {
        $data = $this->getHttpData();
        $id = $data['id'];
        $city = $data['city'];
        $address = $data['address'];
        $zip_code = $data['zip_code'];
        $description = $data['description'];

        $location = new Location();
        return json_encode($location->update($id,$city,$address,$zip_code,$description));
    }
}

?>