<?php
require_once 'controllers/Controller.php';
require_once 'models/Speaker.php';

class SpeakerController extends Controller {
    //Get all the speakers
    function getAll() {
        $speaker = new speaker();
        return json_encode($speaker->getAll());
    }

    //Create a new Speaker
    function create(){
        $firstName = $_POST['first_name'];
        $lastName = $_POST['last_name'];
        $description = $_POST['description'];
        //Pour éviter les requétes JS
        $firstName = htmlspecialchars($firstName);
        $lastName = htmlspecialchars($lastName);
        $description = htmlspecialchars($description);
        
        $speaker = new Speaker();
        return json_encode($speaker->create($firstName,$lastName,$description));
    }

    //Update
    function update() {
        $data = $this->getHttpData();
        $id=$data['id'];
        $firstName = $data['first_name'];
        $lastName = $data['last_name'];
        $description= $data['description'];
        $speaker = new Speaker();
        return json_encode($speaker->update($id,$firstName,$lastName,$description));
    }

    //Delete
    function delete($id) {
        $speaker = new Speaker();
        return json_encode($speaker->delete($id));
    }

}

?>