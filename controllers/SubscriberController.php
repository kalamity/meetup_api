<?php
require_once 'controllers/Controller.php';
require_once 'models/Subscriber.php';

class SubscriberController extends Controller {
    function getAll() {
        $subscriber = new Subscriber();
        return json_encode($subscriber->getAll());
    }

    function create(){
        $firstName = $_POST['first_name'];
        $lastName = $_POST['last_name'];
        $mailAddr = $_POST['mail_addr'];
        //Pour éviter les requétes JS
        $firstName = htmlspecialchars($firstName);
        $lastName = htmlspecialchars($lastName);
        $mailAddr = htmlspecialchars($mailAddr);

        $subscriber = new Subscriber();
        return json_encode($subscriber->create($firstName,$lastName,$mailAddr));
    }

    //Update
    function update() {
        $data = $this->getHttpData();
        $id = $data['id'];
        $firstName = $data['first_name'];
        $lastName = $data['last_name'];
        $mailAddr = $data['mail_addr'];
        
        $subscriber = new Subscriber();
        return json_encode($subscriber->update($id,$firstName,$lastName,$mailAddr));
    }

    //Delete
    function delete($id) {
        $subscriber = new Subscriber();
        return json_encode($subscriber->delete($id));
    }
}

?>