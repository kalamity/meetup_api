<?php
include_once './DB/Db.php';
class Location extends Db {
 // Variables
 private $address;
 private $city;
 private $zip_code;
 private $descritpion;

 //Get Function
    public function getAll() {
        $bdd = Db::connect(); //On utilise  :: avec une class static
        $req = $bdd->prepare('SELECT * FROM location');
        $req->execute();
        return $req->fetchAll();
        }

    public function getAddress() {
        return $this->address;
    }

    public function getCity() {
        return $this->city;
    }

    public function getZipCode() {
        return $this->zip_code;
    }

    public function getDescritpion() {
        return $this->description;
    }

    //Set Function
    public function create($address, $city, $zip_code,$description){
        $bdd = Db::connect();
        $req = $bdd->prepare('INSERT INTO location VALUES (NULL,:city,:address,:zip_code,:description)');
        $req -> execute(['city'=>$city,'address'=>$address,'zip_code'=>$zip_code,'description'=>$description]);
        return [
            'id' => $bdd->lastInsertId(),
            'city'=>$city,
            'address'=>$address,
            'zip_code'=>$zip_code,
            'description'=>$description
            ];
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    public function setcity($city) {
        $this->city = $city;
    }

    public function setzip_code($zip_code) {
        $this->zip_code = $zip_code;
    }

    public function setDescritpion($descritpion) {
        $this->descritpion = $descritpion;
    }

    //Update Function
    public function update($id,$city,$address,$zip_code,$description){
        $bdd = Db::connect();
        $req = $bdd->prepare('UPDATE location SET city=:city, address=:address, zip_code=:zip_code, description=:description WHERE id = :id ');
        $req->execute(['id' => $id,'city'=>$city,'address'=>$address,'zip_code'=>$zip_code,'description'=>$description]);
        return true;
    }
    

    //Delete Function
    public function delete($id) {
        $bdd = Db::connect();
        $req = $bdd->prepare('DELETE FROM location WHERE id = :id');
        $req->execute(['id' => $id]);
        return true;
    }
}
?>