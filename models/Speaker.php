<?php
include_once './DB/Db.php';
class Speaker {

    //Variables
    private $firstName;
    private $lastName;
    private $description;

    //Get Function
    public function getAll() {
        $bdd = Db::connect();
        $req = $bdd->prepare('SELECT * FROM speaker');
        $req->execute();
        return $req->fetchAll();
        }

    public function getFirstName() {
        return $this->firstName;
    }
    public function getLastName() {
        return $this->lastName;
    }
    public function getDescritpion() {
        return $this->description;
    }
 
    //Set Function
    public function create($firstName, $lastName, $description){
        $bdd = Db::connect();
        $req = $bdd->prepare('INSERT INTO speaker VALUES (NULL,:first_name,:last_name,:description)');
        $req -> execute(['first_name'=>$firstName,'last_name'=>$lastName,'description'=>$description]);
        return [
            'id' => $bdd->lastInsertId(),
            'first_name'=>$firstName,
            'last_name'=>$lastName,
            'description'=>$description
        ];
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    public function setdescription($description) {
        $this->description = $description;
    }

    //Update Function
    public function update($id,$firstName,$lastName,$description){
        $bdd = Db::connect();
        $req = $bdd->prepare('UPDATE speaker SET first_name=:first_name,last_name=:last_name, description=:description WHERE id = :id');
        $req->execute(['id' => $id,'first_name'=>$firstName,'last_name'=>$lastName,'description'=>$description ]);
        return true;
    }
    
    //Delete Function
    public function delete($id){
        $bdd = Db::connect();
        $req = $bdd->prepare('DELETE FROM speaker WHERE id = :id');
        $req->execute(['id' => $id]);
        return true;
    }

}

