<?php
include_once './DB/Db.php';
class Meetup extends Db{
    private $title;
    private $description;
    private $image;
    private $date;
    private $location;
    private $subscriber;
    private $speaker;

    //Get functions
    public function getAll(){
        $bdd = Db::connect();
        $req = $bdd->prepare('SELECT * FROM meetup');
        $req->execute();
        return $req->fetchAll();
    }

    public function getMeetup($id){
        $bdd = Db::connect();
        $req = $bdd->prepare('SELECT * FROM meetup WHERE id =:id');
        $req->execute(['id' => $id]);
        return $req->fetch();
    }

    public function getTitle() {
        return $this->title;
    }
    public function getdescription() {
        return $this->description;
    }
    public function getImage() {
        return $this->image;
    }
    public function getDate() {
        return $this->date;
    }
    public function getSubscriber() {
        return $this->subscriber;
    }
    public function getSpeaker() {
        return $this->speaker;
    }

    //Create functions
    public function create($title,$speaker,$description,$location, $image, $date){
        $bdd = Db::connect();
        $req = $bdd->prepare('INSERT INTO meetup (title,speaker,description,location,image,date) VALUES (:title,:speaker,:description,:location,:image,:date)');
        $req -> execute(['title'=>$title,'speaker'=>$speaker,'description'=>$description,'location'=>$location,'image'=>$image,'date'=>$date]);
        return [
            'id' => $bdd->lastInsertId(),
            'title'=>$title,
            'speaker'=>$speaker,
            'description'=>$description,
            'location'=>$location,
            'image'=>$image,
            'date'=>$date,
            ];
    }
    public function setTitle($title) {
        $this->title = $title;
    }

    public function setdescritpion($description) {
        $this->descritpion = $description;
    }

    public function setImage($image) {
        $this->image = $image;
    }

     //Update Function
     public function update($id,$title,$speaker,$description,$location,$image,$date){
        $bdd = Db::connect();
        $req = $bdd->prepare('UPDATE meetup SET title=:title,speaker=:speaker,description=:description, location=:location,image=:image, date=:date WHERE id =:id');
        $req->execute(['id'=>$id,'title'=>$title,'speaker'=>$speaker,'description'=>$description,'location'=>$location,'image'=>$image,'date'=>$date]);
        return true;
    }
    
    //Delete Function
    public function delete($id) { 
        $bdd = Db::connect();
        $req = $bdd->prepare('DELETE FROM meetup WHERE id = :id');
        $req->execute(['id' => $id]);
        return true;
    }

}
?>