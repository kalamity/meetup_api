<?php
include_once './DB/Db.php';
 // Variables
class Subscriber extends Db {
    private $firstName;
    private $lastName;
    private $mailAddr;
    private $dateSubscription;

    //Get Function
    public function getAll() {
        $bdd = Db::connect();
        $req = $bdd->prepare('SELECT * FROM subscriber');
        $req->execute();
        return $req->fetchAll();
    }

    public function getFirstName() {
        return $this->firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function getMailAddr() {
        return $this->mailAddr;
    }

    public function getDateSubscription() {
        return $this->dateSubscription;
    }

    //Create functions
    public function create($firstName, $lastName,$mailAddr){
        $bdd = Db::connect();
        $req = $bdd->prepare('INSERT INTO subscriber VALUES (NULL,:first_name,:last_name,:mail_addr)');
        $req -> execute(['first_name'=>$firstName,'last_name'=>$lastName,'mail_addr'=>$mailAddr]);
        return $req;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    public function setMailAddr($mailAddr) {
        $this->mailAddr = $mailAddr;
    }

    public function setDateSubscription($dateSubscription) {
        $this->dateSubscription = $dateSubscription;
    }

    //Update Function
    public function update($id,$firstName,$lastName,$mailAddr){
        $bdd = Db::connect();
        $req = $bdd->prepare('UPDATE subscriber SET first_name=:first_name, last_name=:last_name, mail_addr=:mail_addr WHERE id =:id');
        $req->execute(['id'=>$id,'first_name'=>$firstName,'last_name'=>$lastName,'mail_addr'=>$mailAddr]);
        return true;
    }
    
    //Delete Function
    public function delete($id) {
        $bdd = Db::connect();
        $req = $bdd->prepare('DELETE FROM subscriber WHERE id = :id');
        $req->execute(['id' => $id]);
        return true;
    }
}



