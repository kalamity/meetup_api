<?php

use Pecee\Http\Middleware\IMiddleware;
use Pecee\Http\Request;
use Pecee\SimpleRouter\SimpleRouter;

class APIMiddleware implements IMiddleware {

    public function handle(Request $request) {
        if($this->isLogged() === false) {
            $url = SimpleRouter::getUrl('login');
            $request->setRewriteUrl($url);
            return $request;
        }
    }

    private function isLogged () {
        SimpleRouter::csrfVerifier(new Pecee\Http\Middleware\BaseCsrfVerifier());
        $token = SimpleRouter::router()->getCsrfVerifier()->getTokenProvider()->getToken();
        if (isset($_GET['token']) && $_GET['token'] === $token || isset($_POST['token']) && $_POST['token'] === $token) {
            return true;
        } else {
            return false;
        }
    }
}