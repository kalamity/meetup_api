<?php

use Pecee\SimpleRouter\SimpleRouter;
require_once 'APIMiddleware.php';
require_once 'controllers/LoginController.php';
require_once 'controllers/DefaultController.php';
require_once 'controllers/SubscriberController.php';
require_once 'controllers/LocationController.php';
require_once 'controllers/SpeakerController.php';
require_once 'controllers/MeetupController.php';


//Prefix qui contient la localisation du site
$prefix = '/projets/meetup_api';

SimpleRouter::group(['prefix' => $prefix], function () {

    SimpleRouter::get('/', 'DefaultController@defaultAction');
    //Subsciber
    SimpleRouter::get('/subscribers', 'SubscriberController@getAll');
    SimpleRouter::post('/subscriber', 'SubscriberController@create');
    SimpleRouter::put('/subscriber/{id}', 'SubscriberController@update');
    SimpleRouter::delete('/subscriber/{id}', 'SubscriberController@delete');
    //Location
    SimpleRouter::get('/locations', 'LocationController@getAll');
    SimpleRouter::post('/location', 'LocationController@create');
    SimpleRouter::delete('/location/{id}', 'LocationController@delete');
    SimpleRouter::put('/location/{id}', 'LocationController@update');
    //Speaker
    SimpleRouter::get('/speakers', 'SpeakerController@getAll');
    SimpleRouter::post('/speaker', 'SpeakerController@create');
    SimpleRouter::delete('/speaker/{id}', 'SpeakerController@delete');
    SimpleRouter::put('/speaker/{id}', 'SpeakerController@update');
    //Meetup
    SimpleRouter::get('/meetups', 'MeetupController@getAll');
    SimpleRouter::post('/meetup', 'MeetupController@create');
    SimpleRouter::get('/meetup/{id}', 'MeetupController@getMeetup');
    SimpleRouter::delete('/meetup/{id}', 'MeetupController@delete');
    SimpleRouter::put('/meetup/{id}', 'MeetupController@update');
});
?>